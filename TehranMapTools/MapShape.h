#ifndef MAPSHAPE_H_INCLUDED
#define MAPSHAPE_H_INCLUDED
#include <list>
#include "TwoDPoint.h"
#define DEFAULT_MAP_COLOR 0
#define MAP_GREEN 1

using namespace std;
class MapShape
{
public:
    MapShape();
    MapShape &operator+=(const TwoDPoint&);
    void setColor(char);
    char getColor() const;
    int getSize() const;
    //virtual void draw() = 0;
    list<TwoDPoint>::iterator getFirstPoint();
    list<TwoDPoint>::iterator getEndPoint();
protected:
    list<TwoDPoint> pointList;
    char color;
};

#endif // MAPSHAPE_H_INCLUDED
