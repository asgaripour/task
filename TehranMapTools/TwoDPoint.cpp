#include "TwoDPoint.h"

TwoDPoint::TwoDPoint(double x, double y)
{
    setLat(x);
    setLon(y);
}

void TwoDPoint::setLat(double x)
{
    lat = x;
}

double TwoDPoint::getLat() const
{
    return lat;
}

void TwoDPoint::setLon(double y)
{
    lon = y;
}

double TwoDPoint::getLon() const
{
    return lon;
}
