#include <windows.h>
#include <gl/Gl.h>
#include <gl/glut.h>

#include "..//TehranMapTools//rapidjson/document.h"
#include "..//TehranMapTools//rapidjson/istreamwrapper.h"
#include "..//TehranMapTools//rapidjson/writer.h"
#include "..//TehranMapTools//rapidjson/stringbuffer.h"
#include "..//TehranMapTools//rapidjson/ostreamwrapper.h"
#include <fstream>
#include <iostream>
#include <iomanip>
#include "..//TehranMapTools//MapConstructor.h"

using namespace rapidjson;
using namespace std;
MapConstructor map;

int	MapConstructor::width{1200};
int MapConstructor::height{600};
int MapConstructor::lastMouseX{0};
int MapConstructor::lastMouseY{0};
int MapConstructor::zoomLevel{1};
int MapConstructor::latOffset{0};
int MapConstructor::lonOffset{0};
int MapConstructor::maxOffset{1500};
float MapConstructor::eyeX{0};
float MapConstructor::eyeY{0};
float MapConstructor::eyeZ{200};
float MapConstructor::eyeHeadDist{0};
float MapConstructor::eyeHeadAngle{0.0f};
bool MapConstructor::truncated{false};

void GlutInit(void)
{
	glClearColor(0.99, 0.99, 0.99, 0.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, 1200, 0, 600);
}

void GlutChangeSize(GLsizei w, GLsizei h)
{
	if(h == 0)
		h = 1;

    glViewport(0, 0, w, h);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glOrtho (-w / 2, w / 2, -h / 2, h / 2, 1.0, -1.0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void GlutDisplay(void)
{
	glClear(GL_COLOR_BUFFER_BIT);

	glBegin(GL_LINES);
    glColor3f(0.96, 0.96, 0.96);
    for(int row = -3000 * MapConstructor::zoomLevel;
            row <= 3000 * MapConstructor::zoomLevel;
            row += 100 * MapConstructor::zoomLevel)
    {
        glVertex2d(row    + MapConstructor::latOffset  * MapConstructor::zoomLevel,
                   (-3000 + MapConstructor::latOffset) * MapConstructor::zoomLevel);
        glVertex2d(row    + MapConstructor::latOffset  * MapConstructor::zoomLevel,
                   (+3000 + MapConstructor::latOffset) * MapConstructor::zoomLevel);

        glVertex2d((-3000 + MapConstructor::lonOffset) * MapConstructor::zoomLevel,
                   row    + MapConstructor::lonOffset  * MapConstructor::zoomLevel);
        glVertex2d((+3000 + MapConstructor::lonOffset) * MapConstructor::zoomLevel,
                   row    + MapConstructor::lonOffset  * MapConstructor::zoomLevel);
    }
    glEnd();

	map.draw();

	glutSwapBuffers();
}

void GlutSpecialKeys(int key, int x, int y)
{
	switch(key)
	{
		case GLUT_KEY_UP:
			MapConstructor::lonOffset -= 2;
			MapConstructor::doOffsetCriteria();
			glutPostRedisplay();
			break;
		case GLUT_KEY_DOWN:
			MapConstructor::lonOffset += 2;
			MapConstructor::doOffsetCriteria();
			glutPostRedisplay();
			break;
		case GLUT_KEY_LEFT:
			MapConstructor::latOffset += 2;
			MapConstructor::doOffsetCriteria();
			glutPostRedisplay();
			break;
		case GLUT_KEY_RIGHT:
			MapConstructor::latOffset -= 2;
			MapConstructor::doOffsetCriteria();
			glutPostRedisplay();
			break;
		case GLUT_KEY_PAGE_UP:
			if(MapConstructor::zoomLevel > 1)
			{
				MapConstructor::zoomLevel--;
				glLineWidth((MapConstructor::zoomLevel + 1) / 2);
				glutPostRedisplay();
			}
			break;
		case GLUT_KEY_PAGE_DOWN:
			if(MapConstructor::zoomLevel < 10)
			{
				MapConstructor::zoomLevel++;
				glLineWidth((MapConstructor::zoomLevel + 1) / 2);
				glutPostRedisplay();
			}
			break;
        case GLUT_KEY_HOME:
            MapConstructor::latOffset = 0;
            MapConstructor::lonOffset = 0;
            MapConstructor::zoomLevel = 1;
			glLineWidth((MapConstructor::zoomLevel + 1) / 2);
			glutPostRedisplay();
			break;
        case GLUT_KEY_F10:
            exit(0);
			break;
	};
}

void GlutMouseAction(int button, int state, int X, int Y)
{
    if(state != GLUT_DOWN)
    {
        MapConstructor::latOffset += (X - MapConstructor::lastMouseX) / MapConstructor::zoomLevel;
        MapConstructor::lonOffset += (Y - MapConstructor::lastMouseY) / MapConstructor::zoomLevel;
        MapConstructor::doOffsetCriteria();
    }
    MapConstructor::lastMouseX = X;
    MapConstructor::lastMouseY = Y;
	glutPostRedisplay();
}

void GlutMotionAction(int X, int Y)
{
    MapConstructor::latOffset += (X - MapConstructor::lastMouseX) / MapConstructor::zoomLevel;
    MapConstructor::lonOffset -= (Y - MapConstructor::lastMouseY) / MapConstructor::zoomLevel;
    MapConstructor::doOffsetCriteria();
    MapConstructor::lastMouseX = X;
    MapConstructor::lastMouseY = Y;
	glutPostRedisplay();
}


int main(int argc, char **argv)
{
    ifstream ifs("E:\\geoData.json");
    if ( !ifs.is_open() )
    {
        cout << "Could not open file for reading!" << endl;
        return EXIT_FAILURE;
    }
    IStreamWrapper isw(ifs);
    Document d;
    d.ParseStream(isw);
    if(d.HasParseError())
    {
        cout << "Error  : " << d.GetParseError()  << endl
             << "Offset : " << d.GetErrorOffset() << endl;
        return EXIT_FAILURE;
    }
    map.loadShapeList(d);
    map.printData();

    glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(1200,600);
	glutInitWindowPosition(50, 50);
	glutCreateWindow("Tehran Map");
	GlutInit();
	glutReshapeFunc(GlutChangeSize);
	glutDisplayFunc(GlutDisplay);
	glutSpecialFunc(GlutSpecialKeys);
	glutMouseFunc(GlutMouseAction);
	glutMotionFunc(GlutMotionAction);
	glutMainLoop();

	return EXIT_SUCCESS;
}
