#include <windows.h>
#include <gl/Gl.h>
#include <gl/glut.h>

#include "..//TehranMapTools//rapidjson/document.h"
#include "..//TehranMapTools//rapidjson/istreamwrapper.h"
#include "..//TehranMapTools//rapidjson/writer.h"
#include "..//TehranMapTools//rapidjson/stringbuffer.h"
#include "..//TehranMapTools//rapidjson/ostreamwrapper.h"
#include <fstream>
#include <iostream>
#include <iomanip>
#include <cmath>
#include "..//TehranMapTools//MapConstructor.h"

using namespace rapidjson;
using namespace std;
MapConstructor map;

int	MapConstructor::width{1200};
int MapConstructor::height{600};
int MapConstructor::lastMouseX{0};
int MapConstructor::lastMouseY{0};
int MapConstructor::zoomLevel{1};
int MapConstructor::latOffset{0};
int MapConstructor::lonOffset{0};
int MapConstructor::maxOffset{1500};
float MapConstructor::eyeX{0};
float MapConstructor::eyeY{0};
float MapConstructor::eyeZ{200};
float MapConstructor::eyeHeadDist{0};
float MapConstructor::eyeHeadAngle{0.0f};
bool MapConstructor::truncated{false};

void GlutInit(void)
{
    glEnable(GL_DEPTH_TEST);
	glFrontFace(GL_CCW);
	glEnable(GL_CULL_FACE);

	glClearColor(0.99f, 0.99f, 0.99f, 0.99f);

    glViewport(0, 0, MapConstructor::width, MapConstructor::height);

    GLfloat fAspect = (GLfloat)MapConstructor::width/(GLfloat)MapConstructor::height;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(90.0f, fAspect, 1.0, 2000.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    MapConstructor::setEyePosition();
}

void GlutChangeSize(GLsizei w, GLsizei h)
{
    GLfloat fAspect;

    if(h == 0)
        h = 1;

    glViewport(0, 0, w, h);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    fAspect = (float)w/(float)h;
    gluPerspective(90.0, fAspect, 1, 2000.0);
    MapConstructor::setEyePosition();
}

void GlutDisplay(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //glRotatef(MapConstructor::eyeHeadAngle, 0, 0, 1);
    glBegin(GL_LINES);
    glColor3f(0.96, 0.96, 0.96);
    for(int row = -4000; row <= 4000; row += 100)
    {
        glVertex3d(row, -4000, 0);
        glVertex3d(row, +4000, 0);
        glVertex3d(-4000, row, 0);
        glVertex3d(+4000, row, 0);
    }
    glEnd();
	map.draw3D();

	glutSwapBuffers();
}

void GlutSpecialKeys(int key, int x, int y)
{
	switch(key)
	{
		case GLUT_KEY_UP:
            MapConstructor::eyeY += 5 * cos(MapConstructor::eyeHeadAngle);
            MapConstructor::eyeX += 5 * sin(MapConstructor::eyeHeadAngle);
            MapConstructor::doEyeCriteria();
            MapConstructor::setEyePosition();
            glutPostRedisplay();
			break;
		case GLUT_KEY_DOWN:
		    MapConstructor::eyeY -= 5 * cos(MapConstructor::eyeHeadAngle);
            MapConstructor::eyeX -= 5 * sin(MapConstructor::eyeHeadAngle);
            MapConstructor::doEyeCriteria();
            MapConstructor::setEyePosition();
            glutPostRedisplay();
			break;
		case GLUT_KEY_LEFT:
            MapConstructor::eyeX -= 5 * cos(MapConstructor::eyeHeadAngle);
            MapConstructor::eyeY += 5 * sin(MapConstructor::eyeHeadAngle);
            MapConstructor::doEyeCriteria();
            MapConstructor::setEyePosition();
            glutPostRedisplay();
			break;
		case GLUT_KEY_RIGHT:
		    MapConstructor::eyeX += 5 * cos(MapConstructor::eyeHeadAngle);
            MapConstructor::eyeY -= 5 * sin(MapConstructor::eyeHeadAngle);
            MapConstructor::doEyeCriteria();
            MapConstructor::setEyePosition();
            glutPostRedisplay();
			break;
		case GLUT_KEY_F11:
			MapConstructor::eyeHeadAngle += 0.01;
			MapConstructor::setEyePosition();
			glutPostRedisplay();
			break;
		case GLUT_KEY_F12:
			MapConstructor::eyeHeadAngle -= 0.01;
			MapConstructor::setEyePosition();
			glutPostRedisplay();
			break;
		case GLUT_KEY_PAGE_UP:
            MapConstructor::eyeZ += 10;
            MapConstructor::doEyeCriteria();
            MapConstructor::setEyePosition();
            glutPostRedisplay();
			break;
		case GLUT_KEY_PAGE_DOWN:
            MapConstructor::eyeZ -= 10;
            MapConstructor::doEyeCriteria();
            MapConstructor::setEyePosition();
            glutPostRedisplay();
			break;
        case GLUT_KEY_HOME:
            MapConstructor::eyeX = 0;
            MapConstructor::eyeY = 0;
            MapConstructor::eyeZ = 200;
            MapConstructor::eyeHeadAngle = 0;
            MapConstructor::eyeHeadDist = 0;
            MapConstructor::setEyePosition();
            glutPostRedisplay();
            break;
        case GLUT_KEY_F10:
            exit(0);
			break;
	};
}

void GlutMouseAction(int button, int state, int X, int Y)
{
    if(state == GLUT_DOWN)
    {
        MapConstructor::lastMouseX = X;
        MapConstructor::lastMouseY = Y;
    }
}

void GlutMotionAction(int X, int Y)
{
    MapConstructor::eyeHeadDist += Y - MapConstructor::lastMouseY;
    //MapConstructor::eyeY -= 10;
    MapConstructor::doEyeCriteria();
    MapConstructor::setEyePosition();
    MapConstructor::lastMouseX = X;
    MapConstructor::lastMouseY = Y;
	glutPostRedisplay();
}

int main(int argc, char **argv)
{
    //Change the address to the complete address of json file on your machine
    ifstream ifs("E://geoData.json");
    if(!ifs.is_open())
    {
        cout << "Could not open file for reading!" << endl;
        return EXIT_FAILURE;
    }
    IStreamWrapper isw(ifs);
    Document d;
    d.ParseStream(isw);
    if(d.HasParseError())
    {
        cout << "Error  : " << d.GetParseError()  << endl
             << "Offset : " << d.GetErrorOffset() << endl;
        return EXIT_FAILURE;
    }
    map.loadShapeList(d);
    map.printData();

    glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(1200, 600);
	glutInitWindowPosition(50, 50);
	glutCreateWindow("Tehran Map");
	GlutInit();
	glutReshapeFunc(GlutChangeSize);
	glutDisplayFunc(GlutDisplay);
	glutSpecialFunc(GlutSpecialKeys);
	glutMouseFunc(GlutMouseAction);
	glutMotionFunc(GlutMotionAction);
	glutMainLoop();

	return EXIT_SUCCESS;
}
