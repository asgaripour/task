#include "MapConstructor.h"
#include <cmath>

MapConstructor::MapConstructor()
    :lineStringList(),
     polygonList(){ }

MapConstructor::MapConstructor(Document &d)
    :lineStringList(),
     polygonList()
{
    loadShapeList(d);
}

void MapConstructor::loadShapeList(Document &d)
{
    Value& featuresList = d["features"];
    TwoDPoint vertex;
    for(unsigned int i = 0; i < featuresList.Size(); i++)
    {
        Value& feature = featuresList[i];
        Value& inputShape = feature["geometry"];
        string shapeType = inputShape["type"].GetString();
        if(shapeType == "LineString")
        {
            LineString *lineString = new LineString();
            Value& pointList = inputShape["coordinates"];
            for(unsigned int j = 0; j < pointList.Size(); j++)
            {
                vertex.setLat(pointList[j][0].GetDouble());
                vertex.setLon(pointList[j][1].GetDouble());
                if(!truncated || isInRange(vertex))
                    (*lineString) += vertex;
            }
            if(!truncated || lineString->getSize())
                (*this) += lineString;
            //else
            //    cout << "Something completely out of range" << endl;
        }
        else if(shapeType == "Polygon")
        {
            Polygonal *polygon = new Polygonal();
            Value& pointList = inputShape["coordinates"][0];
            for(unsigned int j = 0; j < pointList.Size(); j++)
            {
                vertex.setLat(pointList[j][0].GetDouble());
                vertex.setLon(pointList[j][1].GetDouble());
                if(!truncated || isInRange(vertex))
                    (*polygon) += vertex;
            }
            Value& properties = feature["properties"];
            if(properties.HasMember("leisure"))
                if(properties["leisure"] == "park")
                    polygon->setColor(MAP_GREEN);
            if(properties.HasMember("surface"))
                if(properties["surface"] == "grass")
                    polygon->setColor(MAP_GREEN);
            if(!truncated || polygon->getSize())
                (*this) += polygon;
            //else
            //    cout << "Something completely out of range" << endl;
        }
        else
            throw ("Invalid input type.");
    }
}

MapConstructor& MapConstructor::operator+=(LineString *shape)
{
    lineStringList.push_back(shape);
    return *this;
}

MapConstructor& MapConstructor::operator+=(Polygonal *shape)
{
    polygonList.push_back(shape);
    return *this;
}

void MapConstructor::draw() const
{
    //glColor3f(0.95, 0.95, 0.95);
    for(auto shape = polygonList.begin(); shape != polygonList.end(); ++shape)
    {
        if((*shape)->getColor() == MAP_GREEN)
            glColor3ub(197, 232, 197);
        else
            glColor3f(0.95, 0.95, 0.95);
        glBegin(GL_POLYGON);
        auto endPoint = (*shape)->getEndPoint();
        for(auto point = (*shape)->getFirstPoint(); point != endPoint; ++point)
            glVertex2f(calX(point->getLat()), calY(point->getLon()));
        glEnd();
    }

    glColor3ub(255, 200, 100);
    for(auto shape = lineStringList.begin(); shape != lineStringList.end(); ++shape)
    {
        glBegin(GL_LINE_STRIP);
        auto endPoint = (*shape)->getEndPoint();
        for(auto point = (*shape)->getFirstPoint(); point != endPoint; ++point)
            glVertex2f(calX(point->getLat()), calY(point->getLon()));
        glEnd();
    }
}

void MapConstructor::draw3D() const
{
    //glColor3f(0.95, 0.95, 0.95);
    for(auto shape = polygonList.begin(); shape != polygonList.end(); ++shape)
    {
        if((*shape)->getColor() == MAP_GREEN)
            glColor3ub(197, 232, 197);
        else
            glColor3f(0.95, 0.95, 0.95);
        glBegin(GL_POLYGON);
        auto endPoint = (*shape)->getEndPoint();
        for(auto point = (*shape)->getFirstPoint(); point != endPoint; ++point)
            glVertex3f(calX3D(point->getLat()), calY3D(point->getLon()), 0.1);
        glEnd();
    }

    glColor3ub(255, 200, 100);
    for(auto shape = lineStringList.begin(); shape != lineStringList.end(); ++shape)
    {
        glBegin(GL_LINE_STRIP);
        auto endPoint = (*shape)->getEndPoint();
        for(auto point = (*shape)->getFirstPoint(); point != endPoint; ++point)
            glVertex3f(calX3D(point->getLat()), calY3D(point->getLon()), 0.2);
        glEnd();
    }
}

void MapConstructor::setEyePosition()
{
    float centerX = MapConstructor::eyeX + eyeHeadDist * sin(eyeHeadAngle);
    float centerY = MapConstructor::eyeY + eyeHeadDist * cos(eyeHeadAngle);
    float centerZ = 0;
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
	gluLookAt(MapConstructor::eyeX, MapConstructor::eyeY, MapConstructor::eyeZ,
                centerX, centerY, centerZ, sin(eyeHeadAngle), cos(eyeHeadAngle), 0);
}

void MapConstructor::printData() const
{
    cout << lineStringList.size() << " line segments and "
         << polygonList.size() << " polygons are loaded successfully." << endl;
}

double MapConstructor::calX(double x)
{
    //return zoomLevel * ((x - minLat) / (maxLat - minLat) * 1050 - 525.0 + latOffset);
    return zoomLevel * (((x - minLat) / (maxLat - minLat) - 0.5) *
                        1200 +
                        latOffset);
}

double MapConstructor::calY(double y)
{
    //return zoomLevel * ((y - minLon) / (maxLon - minLon) * 600 - 300.0 + lonOffset);
    return zoomLevel * (((y - minLon) / (maxLon - minLon) - 0.5) *
                        1200 * (maxLon - minLon) / (maxLat - minLat) +
                        lonOffset);
}

double MapConstructor::calX3D(double x)
{
    return ((x - minLat) / (maxLat - minLat) - 0.5) *
            1200;
}

double MapConstructor::calY3D(double y)
{
    return ((y - minLon) / (maxLon - minLon) - 0.5) *
            1200 * (maxLon - minLon) / (maxLat - minLat);
}

void MapConstructor::doOffsetCriteria()
{
    if(latOffset > maxOffset)
        latOffset = maxOffset;
    if(latOffset < -maxOffset)
        latOffset = -maxOffset;
    if(lonOffset > maxOffset)
        lonOffset = maxOffset;
    if(lonOffset < -maxOffset)
        lonOffset = -maxOffset;
}

void MapConstructor::doEyeCriteria()
{
    if(eyeX > maxOffset)
        eyeX = maxOffset;
    if(eyeX < -maxOffset)
        eyeX = -maxOffset;
    if(eyeY > maxOffset)
        eyeY = maxOffset;
    if(eyeY < -maxOffset)
        eyeY = -maxOffset;
    if(eyeZ > 400)
        eyeZ = 400;
    if(eyeZ < 20)
        eyeZ = 20;
    if(eyeHeadDist > 300)
        eyeHeadDist = 300;
    if(eyeHeadDist < 0)
        eyeHeadDist = 0;
}

bool MapConstructor::isInRange(TwoDPoint v)
{
    return  v.getLat() >= minLat &&
            v.getLat() <= maxLat &&
            v.getLon() >= minLon &&
            v.getLon() <= maxLon;
}

MapConstructor::~MapConstructor()
{
    for(auto shape = lineStringList.begin(); shape != lineStringList.end(); ++shape)
        delete (*shape);
    for(auto shape = polygonList.begin(); shape != polygonList.end(); ++shape)
        delete (*shape);
    lineStringList.clear();
    polygonList.clear();
}
