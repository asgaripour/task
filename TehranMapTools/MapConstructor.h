#ifndef MAPSHAPELIST_H_INCLUDED
#define MAPSHAPELIST_H_INCLUDED

#include <exception>
#include <list>
#include <iostream>
#include "LineString.h"
#include "Polygon.h"
#include "rapidjson/document.h"

using namespace std;
using namespace rapidjson;

class MapConstructor
{
public:
    MapConstructor();
    MapConstructor(Document &d);
    ~MapConstructor();
    void loadShapeList(Document &d);
    //MapConstructor &operator+=(MapShape*);
    MapConstructor& operator+=(Polygonal*);
    MapConstructor& operator+=(LineString*);
    void draw() const;
    void draw3D() const;
    void printData() const;
    static double calX(double);
    static double calY(double);
    static double calX3D(double);
    static double calY3D(double);
    static void doOffsetCriteria();
    static void doEyeCriteria();
    static void setEyePosition();

    static int lastMouseX;
    static int lastMouseY;
    static int width;
    static int height;

    static int zoomLevel;
    static int latOffset;
    static int lonOffset;
    static int maxOffset;

    static float eyeX;
    static float eyeY;
    static float eyeZ;
    static float eyeHeadDist;
    static float eyeHeadAngle;

    static bool truncated;
private:
    /*
    static constexpr double minLat = 51.377342;
    static constexpr double maxLat = 51.410731;
    static constexpr double minLon = 35.689496;
    static constexpr double maxLon = 35.714031;
    */
    static constexpr double minLat = 51.355;
    static constexpr double maxLat = 51.435;
    static constexpr double minLon = 35.68;
    static constexpr double maxLon = 35.72;

    static bool isInRange(TwoDPoint);
    //list<MapShape*> shapeList;
    list<LineString*> lineStringList;
    list<Polygonal*> polygonList;
};

#endif // MAPSHAPELIST_H_INCLUDED
