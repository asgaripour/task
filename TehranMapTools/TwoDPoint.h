#ifndef TWODPOINT_H_INCLUDED
#define TWODPOINT_H_INCLUDED

class TwoDPoint
{
public:
    TwoDPoint(double = 0.0, double = 0.0);
    void setLat(double);
    double getLat() const;
    void setLon(double);
    double getLon() const;
private:
    double lat;
    double lon;
};


#endif // TWODPOINT_H_INCLUDED
