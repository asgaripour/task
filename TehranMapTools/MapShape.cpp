#include "MapShape.h"

MapShape::MapShape()
    :pointList()
{
    setColor(DEFAULT_MAP_COLOR);
}

MapShape& MapShape::operator+=(const TwoDPoint& p)
{
    pointList.push_back(p);
    return *this;
}

void MapShape::setColor(char col)
{
    color = col;
}

char MapShape::getColor() const
{
    return color;
}

int MapShape::getSize() const
{
    return pointList.size();
}

list<TwoDPoint>::iterator MapShape::getFirstPoint()
{
    return pointList.begin();
}

list<TwoDPoint>::iterator MapShape::getEndPoint()
{
    return pointList.end();
}
